Project based on .NET Core 2.2

After run this project, it run on: https://localhost:44344

Test if project worked, using API: 
	https://localhost:44344/reservation/get
	Method: GET
	It will return string data "Connected!".

API get one record:
	https://localhost:44344/reservation/{id}
	Method: GET
	Example: https://localhost:44344/reservation/b704c069-d746-4f6b-846d-5d536ff1a228

API get many records:
	https://localhost:44344/reservation/getrecordbyids
	Method: POST
	Data format: application/json
	Data: IEnumerable<Guid>
	Example data: [
"b704c069-d746-4f6b-846d-5d536ff1a228",
"2e4443be-6f98-4023-af5f-813b23857a43"
]

API create one record:
	https://localhost:44344/reservation/createrecord
	Method: POST
	Data format: application/json
	Data: <Reservation Model>
	Example data: in file ExampleData/create_one_record.json

API create many records:
	https://localhost:44344/reservation/createrecords
	Method: POST
	Data format: application/json
	Data: IEnumerable<Reservation Model>
	Example data: in file ExampleData/create_many_records.json