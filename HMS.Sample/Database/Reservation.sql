CREATE TABLE `Reservation` (
  `ID` char(36) NOT NULL,
  `ArrivalDate` datetime NOT NULL,
  `DepartureDate` datetime NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL,
  `PropertyID` char(36) NOT NULL,
  `PropertyCode` varchar(256) NOT NULL,
  `PropertyName` varchar(256) NOT NULL,
  `BookingDate` datetime NOT NULL,
  `NumberOfRooms` tinyint(1) unsigned NOT NULL,
  `CancellationDate` datetime DEFAULT NULL,
  `NumberOfAdults` tinyint(1) unsigned NOT NULL,
  `RoomConfirmationNumber` varchar(36) DEFAULT NULL,
  `ItineraryNumber` varchar(36) DEFAULT NULL,
  `Total` decimal(13,4) NOT NULL,
  `CurrencyCode` char(3) NOT NULL,
  `PromotionCode` varchar(256) DEFAULT NULL,
  `DistributionChannel` varchar(36) NOT NULL,
  `TravelType` tinyint(1) unsigned NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `ProfileNames` text,
  `ProfileEmails` text,
  `ProfilePhones` text,
  `ProfileReferenceIDs` text,
  `RoomTypeCodes` text,
  `RoomTypeNames` text,
  `RatePlanCodes` text,
  `RatePlanNames` text,
  `NumberOfOtherOccupancies` tinyint(1) unsigned DEFAULT NULL,
  `ReservationReferenceIDs` text,
  PRIMARY KEY (`ID`),
  KEY `ItineraryIndex` (`ItineraryNumber`),
  KEY `RoomConfirmationNumberIndex` (`RoomConfirmationNumber`)
);

CREATE TABLE `ReservationCancellationInfo` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `CancellationPolicyRefID` char(36) DEFAULT NULL,
  `CancellationPolicyName` varchar(256) DEFAULT NULL,
  `IsRefundable` bit(1) DEFAULT NULL,
  `CancelPenaltyAmount` decimal(13,4) unsigned DEFAULT NULL,
  `CancelPenaltyPercent` decimal(3,2) unsigned DEFAULT NULL,
  `CancelPenaltyNumberNights` tinyint(3) unsigned DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_CancellationInfo_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_CancellationInfo_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationCancellationPolicy` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `CancellationPolicyRefID` char(36) DEFAULT NULL,
  `CancellationPolicyName` varchar(256) DEFAULT NULL,
  `CancellationDescription` text,
  `CanCancel` bit(1) DEFAULT NULL,
  `NonRefundable` bit(1) DEFAULT NULL,
  `HourThresholdBeforeReservation` int(10) unsigned DEFAULT NULL,
  `CancelPenaltyAmount` decimal(13,4) DEFAULT NULL,
  `CancelPenaltyPercent` decimal(3,2) DEFAULT NULL,
  `CancelPenaltyNumberNights` tinyint(1) DEFAULT NULL,
  `JsonValue` text,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_CancellationPolicy_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_CancellationPolicy_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationExtra` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `StayDate` datetime DEFAULT NULL,
  `ExtraCode` varchar(256) NOT NULL,
  `ExtraName` varchar(256) DEFAULT NULL,
  `Type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Quantity` int(10) unsigned DEFAULT NULL,
  `RateAmount` decimal(13,4) DEFAULT NULL,
  `JsonValue` text,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Extra_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_Reservation_Extra` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationExtraFee` (
  `ID` char(36) NOT NULL,
  `ReservationExtraID` char(36) NOT NULL,
  `FeeItem` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `FeeType` tinyint(1) unsigned DEFAULT NULL,
  `FeeAmount` decimal(13,4) DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ExtraFee_Extra_idx` (`ReservationExtraID`),
  CONSTRAINT `FK_ExtraFee_Extra` FOREIGN KEY (`ReservationExtraID`) REFERENCES `ReservationExtra` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationExtraTax` (
  `ID` char(36) NOT NULL,
  `ReservationExtraID` char(36) NOT NULL,
  `TaxItem` varchar(256) DEFAULT NULL,
  `TaxType` tinyint(1) unsigned DEFAULT NULL,
  `TaxAmount` decimal(13,4) DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ExtraTax_Extra_idx` (`ReservationExtraID`),
  CONSTRAINT `FK_ExtraTax_Extra` FOREIGN KEY (`ReservationExtraID`) REFERENCES `ReservationExtra` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationFee` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `FeeItem` varchar(256) DEFAULT NULL,
  `FeeType` tinyint(1) unsigned DEFAULT NULL,
  `FeeAmount` decimal(13,4) DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ReservationFee_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_ReservationFee_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationGuaranteeInfo` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `GuaranteeRefID` char(36) DEFAULT NULL,
  `GuaranteeName` varchar(256) DEFAULT NULL,
  `GuaranteeType` tinyint(1) unsigned NOT NULL,
  `GuaranteeMethod` tinyint(1) unsigned NOT NULL,
  `GuaranteeValue` text COMMENT 'json format for guarantee value regards to guarantee method',
  `GuaranteeDescription` text,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_GuaranteeInfo_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_GuaranteeInfo_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationGuaranteePolicy` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `GuaranteePolicyRefID` char(36) DEFAULT NULL,
  `GuaranteePolicyName` varchar(256) DEFAULT NULL,
  `GuaranteePolicyDescription` text,
  `JsonValue` text,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  `DepositAmount` decimal(13,4) DEFAULT NULL,
  `DepositPercent` decimal(3,2) DEFAULT NULL,
  `DepositNumberNights` tinyint(1) unsigned DEFAULT NULL,
  `DepositTiming` tinyint(1) unsigned DEFAULT NULL,
  `DepositDays` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_GuaranteePolicy_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_GuaranteePolicy_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationLog` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `Action` tinyint(1) NOT NULL,
  `Notes` text,
  `UserRefID` char(36) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Log_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_Log_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationOtherOccupancy` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `OccupancyRefID` char(36) DEFAULT NULL,
  `OccupancyRefCode` char(36) DEFAULT NULL,
  `Quantity` tinyint(1) unsigned DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_OtherOccupancy_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_OtherOccupancy_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationProfile` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `ProfileRefID` char(36) DEFAULT NULL,
  `ProfileType` tinyint(1) unsigned NOT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `MiddleName` varchar(255) DEFAULT NULL,
  `PassportID` varchar(255) DEFAULT NULL,
  `PassportExpiredDate` datetime DEFAULT NULL,
  `GenderType` tinyint(1) unsigned DEFAULT NULL,
  `Address` longtext,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `PhoneNumber` varchar(255) DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Profile_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_Profile_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationProfileReferenceID` (
  `ID` char(36) NOT NULL,
  `ReservationProfileID` char(36) NOT NULL,
  `ReferenceName` varchar(256) NOT NULL,
  `ReferenceValue` varchar(256) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ProfileReference_Profile_idx` (`ReservationProfileID`),
  CONSTRAINT `FK_ProfileReference_Profile` FOREIGN KEY (`ReservationProfileID`) REFERENCES `ReservationProfile` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationPropertyInfo` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `JsonValue` text NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_ReservationPropertyInfo_Reservation_idx` (`ReservationID`),
  CONSTRAINT `fk_ReservationPropertyInfo_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRoomRate` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `StayDate` datetime NOT NULL,
  `RoomTypeCode` varchar(256) NOT NULL,
  `RoomTypeName` varchar(256) DEFAULT NULL,
  `RoomTypeRefID` char(36) DEFAULT NULL,
  `RatePlanCode` varchar(256) NOT NULL,
  `RatePlanName` varchar(256) DEFAULT NULL,
  `RatePlanRefID` char(36) DEFAULT NULL,
  `RateAmount` decimal(13,4) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RoomRate_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_RoomRate_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRatePlanInfo` (
  `ID` char(36) NOT NULL,
  `ReservationRoomRateID` char(36) NOT NULL,
  `RatePlanName` varchar(256) DEFAULT NULL,
  `RatePlanCode` varchar(256) NOT NULL,
  `RatePlanDescription` text,
  `JsonValue` text,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RatePlanInfo_RoomRate_idx` (`ReservationRoomRateID`),
  CONSTRAINT `FK_RatePlanInfo_RoomRate` FOREIGN KEY (`ReservationRoomRateID`) REFERENCES `ReservationRoomRate` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationReferenceID` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `Type` tinyint(1) unsigned NOT NULL,
  `Value` varchar(36) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_ReferenceID_Reservation` (`ReservationID`),
  CONSTRAINT `FK_ReferenceID_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRequest` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `RequestType` tinyint(1) unsigned NOT NULL,
  `RequestContent` text NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Request_Reservation` (`ReservationID`),
  CONSTRAINT `FK_Request_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRoomRateFee` (
  `ID` char(36) NOT NULL,
  `ReservationRoomRateID` char(36) NOT NULL,
  `FeeItem` varchar(256) NOT NULL,
  `FeeType` tinyint(1) unsigned NOT NULL,
  `FeeAmount` decimal(13,4) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RoomRateFee_RoomRate` (`ReservationRoomRateID`),
  CONSTRAINT `FK_RoomRateFee_RoomRate` FOREIGN KEY (`ReservationRoomRateID`) REFERENCES `ReservationRoomRate` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRoomRateTax` (
  `ID` char(36) NOT NULL,
  `ReservationRoomRateID` char(36) NOT NULL,
  `TaxItem` varchar(256) NOT NULL,
  `TaxType` tinyint(1) unsigned NOT NULL,
  `TaxAmount` decimal(13,4) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RoomRateTax_RoomRate` (`ReservationRoomRateID`),
  CONSTRAINT `FK_RoomRateTax_RoomRate` FOREIGN KEY (`ReservationRoomRateID`) REFERENCES `ReservationRoomRate` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRoomTypeInfo` (
  `ID` char(36) NOT NULL,
  `ReservationRoomRateID` char(36) NOT NULL,
  `RoomTypeName` varchar(256) DEFAULT NULL,
  `RoomTypeCode` varchar(256) NOT NULL,
  `RoomTypeDescription` text,
  `JsonValue` text,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_RoomTypeInfo_RoomRate` (`ReservationRoomRateID`),
  CONSTRAINT `FK_RoomTypeInfo_RoomRate` FOREIGN KEY (`ReservationRoomRateID`) REFERENCES `ReservationRoomRate` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationRoomTypeAmenityInfo` (
  `ID` char(36) NOT NULL,
  `ReservationRoomTypeInfoID` char(36) NOT NULL,
  `Type` tinyint(1) DEFAULT NULL,
  `Name` varchar(256) DEFAULT NULL,
  `Code` varchar(256) NOT NULL,
  `RefID` char(36) DEFAULT NULL,
  `Description` text,
  `CreatedDate` datetime NOT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_reservationRoomTypeInfo_reservationRoomTypeAmenityInfo_idx` (`ReservationRoomTypeInfoID`),
  CONSTRAINT `FK_RoomTypeAmenity_RoomTypeInfo` FOREIGN KEY (`ReservationRoomTypeInfoID`) REFERENCES `ReservationRoomTypeInfo` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE `ReservationTax` (
  `ID` char(36) NOT NULL,
  `ReservationID` char(36) NOT NULL,
  `TaxTtem` varchar(256) DEFAULT NULL,
  `TaxType` tinyint(1) unsigned DEFAULT NULL,
  `TaxAmount` decimal(13,4) DEFAULT NULL,
  `CreatedTime` datetime NOT NULL,
  `UpdatedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_Tax_Reservation_idx` (`ReservationID`),
  CONSTRAINT `FK_Tax_Reservation` FOREIGN KEY (`ReservationID`) REFERENCES `Reservation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
