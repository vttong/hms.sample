﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationProfileRepository : RepositoryBase<ReservationProfile>, IReservationProfileRepository
    {
        public ReservationProfileRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
