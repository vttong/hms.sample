﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationRoomTypeAmenityInfoRepository : IRepositoryBase<ReservationRoomTypeAmenityInfo>
    {
    }
}
