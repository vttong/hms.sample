﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationExtraRepository : IRepositoryBase<ReservationExtra>
    {
    }
}
