﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRatePlanInfoRepository : RepositoryBase<ReservationRatePlanInfo>, IReservationRatePlanInfoRepository
    {
        public ReservationRatePlanInfoRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
