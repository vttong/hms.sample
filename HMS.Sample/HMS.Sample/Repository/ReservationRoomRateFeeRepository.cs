﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRoomRateFeeRepository : RepositoryBase<ReservationRoomRateFee>, IReservationRoomRateFeeRepository
    {
        public ReservationRoomRateFeeRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
