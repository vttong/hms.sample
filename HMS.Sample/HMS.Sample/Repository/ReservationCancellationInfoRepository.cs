﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationCancellationInfoRepository : RepositoryBase<ReservationCancellationInfo>, IReservationCancellationInfoRepository
    {
        public ReservationCancellationInfoRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
