﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationTaxRepository : RepositoryBase<ReservationTax>, IReservationTaxRepository
    {
        public ReservationTaxRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
