﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationProfileReferenceIDRepository : RepositoryBase<ReservationProfileReferenceID>, IReservationProfileReferenceIDRepository
    {
        public ReservationProfileReferenceIDRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
