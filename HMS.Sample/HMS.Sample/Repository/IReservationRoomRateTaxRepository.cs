﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationRoomRateTaxRepository : IRepositoryBase<ReservationRoomRateTax>
    {
    }
}
