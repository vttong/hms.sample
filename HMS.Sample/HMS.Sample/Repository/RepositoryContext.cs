﻿using HMS.Sample.Models;
using Microsoft.EntityFrameworkCore;

namespace HMS.Sample.Repository
{
    public class RepositoryContext: DbContext
    {
        public RepositoryContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<ReservationCancellationInfo> ReservationCancellationInfos { get; set; }
        public DbSet<ReservationCancellationPolicy> ReservationCancellationPolicies { get; set; }
        public DbSet<ReservationExtra> ReservationExtras { get; set; }
        public DbSet<ReservationExtraFee> ReservationExtraFees { get; set; }
        public DbSet<ReservationExtraTax> ReservationExtraTaxs { get; set; }
        public DbSet<ReservationFee> ReservationFees { get; set; }
        public DbSet<ReservationGuaranteeInfo> ReservationGuaranteeInfos { get; set; }
        public DbSet<ReservationGuaranteePolicy> ReservationGuaranteePolicies { get; set; }
        public DbSet<ReservationLog> ReservationLogs { get; set; }
        public DbSet<ReservationOtherOccupancy> ReservationOtherOccupancies { get; set; }
        public DbSet<ReservationProfile> ReservationProfiles { get; set; }
        public DbSet<ReservationProfileReferenceID> ReservationProfileReferenceIDs { get; set; }
        public DbSet<ReservationPropertyInfo> ReservationPropertyInfos { get; set; }
        public DbSet<ReservationRatePlanInfo> ReservationRatePlanInfos { get; set; }
        public DbSet<ReservationReferenceID> ReservationReferenceIDs { get; set; }
        public DbSet<ReservationRequest> ReservationRequests { get; set; }
        public DbSet<ReservationRoomRate> ReservationRoomRates { get; set; }
        public DbSet<ReservationRoomRateFee> ReservationRoomRateFees { get; set; }
        public DbSet<ReservationRoomRateTax> ReservationRoomRateTaxs { get; set; }
        public DbSet<ReservationRoomTypeAmenityInfo> ReservationRoomTypeAmenityInfos { get; set; }
        public DbSet<ReservationRoomTypeInfo> ReservationRoomTypeInfos { get; set; }
        public DbSet<ReservationTax> ReservationTaxs { get; set; }
    }
}
