﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationRoomRateRepository : IRepositoryBase<ReservationRoomRate>
    {
    }
}
