﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationExtraRepository : RepositoryBase<ReservationExtra>, IReservationExtraRepository
    {
        public ReservationExtraRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
