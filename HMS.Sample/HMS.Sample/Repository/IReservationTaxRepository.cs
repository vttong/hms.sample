﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationTaxRepository : IRepositoryBase<ReservationTax>
    {
    }
}
