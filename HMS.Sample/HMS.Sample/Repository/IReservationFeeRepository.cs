﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationFeeRepository : IRepositoryBase<ReservationFee>
    {
    }
}
