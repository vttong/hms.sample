﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationExtraFeeRepository : IRepositoryBase<ReservationExtraFee>
    {
    }
}
