﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationOtherOccupancyRepository : RepositoryBase<ReservationOtherOccupancy>, IReservationOtherOccupancyRepository
    {
        public ReservationOtherOccupancyRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
