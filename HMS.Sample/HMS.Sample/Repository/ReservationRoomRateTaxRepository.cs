﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRoomRateTaxRepository : RepositoryBase<ReservationRoomRateTax>, IReservationRoomRateTaxRepository
    {
        public ReservationRoomRateTaxRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
