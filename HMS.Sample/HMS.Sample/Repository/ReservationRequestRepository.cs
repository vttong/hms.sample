﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRequestRepository : RepositoryBase<ReservationRequest>, IReservationRequestRepository
    {
        public ReservationRequestRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
