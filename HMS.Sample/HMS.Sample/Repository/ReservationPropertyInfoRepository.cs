﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationPropertyInfoRepository : RepositoryBase<ReservationPropertyInfo>, IReservationPropertyInfoRepository
    {
        public ReservationPropertyInfoRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
