﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationReferenceIDRepository : IRepositoryBase<ReservationReferenceID>
    {
    }
}
