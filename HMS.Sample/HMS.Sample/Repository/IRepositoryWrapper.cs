﻿using System.Threading.Tasks;

namespace HMS.Sample.Repository
{
    public interface IRepositoryWrapper
    {
        IReservationRepository Reservation { get; }
        //IReservationCancellationInfoRepository ReservationCancellationInfo { get; }
        //IReservationCancellationPolicyRepository ReservationCancellationPolicy { get; }
        //IReservationExtraRepository ReservationExtra { get; }
        //IReservationExtraFeeRepository ReservationExtraFee { get; }
        //IReservationExtraTaxRepository ReservationExtraTax { get; }
        //IReservationFeeRepository ReservationFee { get; }
        //IReservationGuaranteeInfoRepository ReservationGuaranteeInfo { get; }
        //IReservationGuaranteePolicyRepository ReservationGuaranteePolicy { get; }
        //IReservationLogRepository ReservationLog { get; }
        //IReservationOtherOccupancyRepository ReservationOtherOccupancy { get; }
        //IReservationProfileRepository ReservationProfile { get; }
        //IReservationProfileReferenceIDRepository ReservationProfileReferenceID { get; }
        //IReservationPropertyInfoRepository ReservationPropertyInfo { get; }
        //IReservationRatePlanInfoRepository ReservationRatePlanInfo { get; }
        //IReservationReferenceIDRepository ReservationReferenceID { get; }
        //IReservationRequestRepository ReservationRequest { get; }
        //IReservationRoomRateRepository ReservationRoomRate { get; }
        //IReservationRoomRateFeeRepository ReservationRoomRateFee { get; }
        //IReservationRoomRateTaxRepository ReservationRoomRateTax { get; }
        //IReservationRoomTypeAmenityInfoRepository ReservationRoomTypeAmenityInfo { get; }
        //IReservationRoomTypeInfoRepository ReservationRoomTypeInfo { get; }
        //IReservationTaxRepository ReservationTax { get; }
        Task<int> SaveAsync();
    }
}
