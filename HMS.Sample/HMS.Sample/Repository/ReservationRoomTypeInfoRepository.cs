﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRoomTypeInfoRepository : RepositoryBase<ReservationRoomTypeInfo>, IReservationRoomTypeInfoRepository
    {
        public ReservationRoomTypeInfoRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
