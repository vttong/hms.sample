﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationRequestRepository : IRepositoryBase<ReservationRequest>
    {
    }
}
