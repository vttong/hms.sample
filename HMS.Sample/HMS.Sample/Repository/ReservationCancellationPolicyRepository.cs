﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationCancellationPolicyRepository : RepositoryBase<ReservationCancellationPolicy>, IReservationCancellationPolicyRepository
    {
        public ReservationCancellationPolicyRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
