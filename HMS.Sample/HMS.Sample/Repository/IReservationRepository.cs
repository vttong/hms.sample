﻿using HMS.Sample.Models;
using System;
using System.Threading.Tasks;

namespace HMS.Sample.Repository
{
    public interface IReservationRepository : IRepositoryBase<Reservation>
    {
        Task<Reservation> GetReservationByIdAsync(Guid id);
    }
}
