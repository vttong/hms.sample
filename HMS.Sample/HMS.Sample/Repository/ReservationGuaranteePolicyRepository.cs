﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationGuaranteePolicyRepository : RepositoryBase<ReservationGuaranteePolicy>, IReservationGuaranteePolicyRepository
    {
        public ReservationGuaranteePolicyRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
