﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRoomRateRepository : RepositoryBase<ReservationRoomRate>, IReservationRoomRateRepository
    {
        public ReservationRoomRateRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
