﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationExtraTaxRepository : RepositoryBase<ReservationExtraTax>, IReservationExtraTaxRepository
    {
        public ReservationExtraTaxRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
