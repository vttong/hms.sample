﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationFeeRepository : RepositoryBase<ReservationFee>, IReservationFeeRepository
    {
        public ReservationFeeRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
