﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationPropertyInfoRepository : IRepositoryBase<ReservationPropertyInfo>
    {
    }
}
