﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationReferenceIDRepository : RepositoryBase<ReservationReferenceID>, IReservationReferenceIDRepository
    {
        public ReservationReferenceIDRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
