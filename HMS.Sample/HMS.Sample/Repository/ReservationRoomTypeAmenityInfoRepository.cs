﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationRoomTypeAmenityInfoRepository : RepositoryBase<ReservationRoomTypeAmenityInfo>, IReservationRoomTypeAmenityInfoRepository
    {
        public ReservationRoomTypeAmenityInfoRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
