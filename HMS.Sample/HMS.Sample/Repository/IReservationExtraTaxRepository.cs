﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationExtraTaxRepository : IRepositoryBase<ReservationExtraTax>
    {
    }
}
