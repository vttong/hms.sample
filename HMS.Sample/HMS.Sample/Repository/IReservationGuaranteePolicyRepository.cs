﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationGuaranteePolicyRepository : IRepositoryBase<ReservationGuaranteePolicy>
    {
    }
}
