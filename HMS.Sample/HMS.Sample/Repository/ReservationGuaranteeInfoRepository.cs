﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationGuaranteeInfoRepository : RepositoryBase<ReservationGuaranteeInfo>, IReservationGuaranteeInfoRepository
    {
        public ReservationGuaranteeInfoRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
