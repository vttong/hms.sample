﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationProfileRepository : IRepositoryBase<ReservationProfile>
    {
    }
}
