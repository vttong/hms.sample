﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationRoomRateFeeRepository : IRepositoryBase<ReservationRoomRateFee>
    {
    }
}
