﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationRatePlanInfoRepository : IRepositoryBase<ReservationRatePlanInfo>
    {
    }
}
