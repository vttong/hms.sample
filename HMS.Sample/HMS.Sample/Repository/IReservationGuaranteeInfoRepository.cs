﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public interface IReservationGuaranteeInfoRepository : IRepositoryBase<ReservationGuaranteeInfo>
    {
    }
}
