﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationExtraFeeRepository : RepositoryBase<ReservationExtraFee>, IReservationExtraFeeRepository
    {
        public ReservationExtraFeeRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
