﻿using HMS.Sample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HMS.Sample.Repository
{
    public interface IReservationCancellationInfoRepository : IRepositoryBase<ReservationCancellationInfo>
    {
    }
}
