﻿using HMS.Sample.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HMS.Sample.Repository
{
    public class ReservationRepository : RepositoryBase<Reservation>, IReservationRepository
    {
        public ReservationRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<Reservation> GetReservationByIdAsync(Guid id)
        {
            return await FindByCondition(x => x.ID.Equals(id))
                .SingleAsync();
        }
    }
}
