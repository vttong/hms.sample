﻿using HMS.Sample.Models;

namespace HMS.Sample.Repository
{
    public class ReservationLogRepository : RepositoryBase<ReservationLog>, IReservationLogRepository
    {
        public ReservationLogRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
