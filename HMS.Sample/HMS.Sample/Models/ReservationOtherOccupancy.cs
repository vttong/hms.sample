﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationotheroccupancy")]
    public class ReservationOtherOccupancy
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public string OccupancyRefID { get; set; }
        public string OccupancyRefCode { get; set; }
        public int Quantity { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
