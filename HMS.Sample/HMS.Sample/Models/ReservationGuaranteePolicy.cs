﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationguaranteepolicy")]
    public class ReservationGuaranteePolicy
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public string GuaranteePolicyRefID { get; set; }
        public string GuaranteePolicyName { get; set; }
        public string GuaranteePolicyDescription { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
        public decimal? DepositAmount { get; set; }
        public decimal? DepositPercent { get; set; }
        public int? DepositNumberNights { get; set; }
        public int? DepositTiming { get; set; }
        public int? DepositDays { get; set; }
        public string JsonValue { get; set; }
    }
}
