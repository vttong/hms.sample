﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationextrafee")]
    public class ReservationExtraFee : FeeBase
    {
        public Guid ReservationExtraID { get; set; }
    }
}
