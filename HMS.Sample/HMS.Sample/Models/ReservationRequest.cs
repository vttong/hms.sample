﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationrequest")]
    public class ReservationRequest
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public RequestType RequestType { get; set; }
        public string RequestContent { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
