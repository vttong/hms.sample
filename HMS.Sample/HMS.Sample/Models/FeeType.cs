﻿namespace HMS.Sample.Models
{
    public enum FeeType
    {
        NotSpecific = 0,
        Inclusive = 1,
        Exclusive = 2
    }
}
