﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationreferenceid")]
    public class ReservationReferenceID
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public ReferenceType Type { get; set; }
        public string Value { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
