﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationlog")]
    public class ReservationLog
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public ReservationAction Action { get; set; }
        public string Notes { get; set; }
        public string UserRefID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
