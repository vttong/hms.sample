﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HMS.Sample.Models
{
    public class FeeBase
    {
        [Key]
        public Guid ID { get; set; }
        public string FeeItem { get; set; }
        public FeeType FeeType { get; set; }
        public decimal FeeAmount { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
