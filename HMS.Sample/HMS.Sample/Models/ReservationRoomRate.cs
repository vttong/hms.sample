﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationroomrate")]
    public class ReservationRoomRate
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public DateTime StayDate { get; set; }
        public string RoomTypeCode { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeRefID { get; set; }
        public string RatePlanCode { get; set; }
        public string RatePlanName { get; set; }
        public string RatePlanRefID { get; set; }
        public decimal RateAmount { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }

        public virtual ReservationRatePlanInfo RatePlanInfo { get; set; }
        public virtual IList<ReservationRoomRateFee> Fees { get; set; }
        public virtual IList<ReservationRoomRateTax> Taxes { get; set; }
        public virtual ReservationRoomTypeInfo RoomTypeInfo { get; set; }
    }
}
