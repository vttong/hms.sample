﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationtax")]
    public class ReservationTax : TaxBase
    {
        public Guid ReservationID { get; set; }
    }
}
