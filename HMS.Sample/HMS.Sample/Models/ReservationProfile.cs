﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationprofile")]
    public class ReservationProfile
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public string ProfileRefID { get; set; }
        public ProfileType ProfileType { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PassportId { get; set; }
        public DateTime PassportExpiredDate { get; set; }
        public GenderType GenderType { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }

        public virtual IList<ReservationProfileReferenceID> ProfileReferenceIDs { get; set; }
    }
}
