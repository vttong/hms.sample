﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationextratax")]
    public class ReservationExtraTax : TaxBase
    {
        public Guid ReservationExtraID { get; set; }
    }
}
