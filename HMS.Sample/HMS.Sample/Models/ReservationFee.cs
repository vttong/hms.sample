﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationfee")]
    public class ReservationFee : FeeBase
    {
        public Guid ReservationID { get; set; }
    }
}
