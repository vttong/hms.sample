﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationroomratetax")]
    public class ReservationRoomRateTax : TaxBase
    {
        public Guid ReservationRoomRateID { get; set; }
    }
}
