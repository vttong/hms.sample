﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HMS.Sample.Models
{
    public class TaxBase
    {
        [Key]
        public Guid ID { get; set; }
        public string TaxItem { get; set; }
        public TaxType TaxType { get; set; }
        public decimal TaxAmount { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
