﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationroomratefee")]
    public class ReservationRoomRateFee : FeeBase
    {
        public Guid ReservationRoomRateID { get; set; }
    }
}
