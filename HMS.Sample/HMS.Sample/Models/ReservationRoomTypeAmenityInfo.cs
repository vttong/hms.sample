﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationroomtypeamenityinfo")]
    public class ReservationRoomTypeAmenityInfo
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationRoomTypeInfoID { get; set; }
        public Guid RefID { get; set; }
        public AmenityType Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
