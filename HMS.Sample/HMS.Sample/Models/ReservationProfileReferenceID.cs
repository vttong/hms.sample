﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationprofilereferenceid")]
    public class ReservationProfileReferenceID
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationProfileID { get; set; }
        public string ReferenceName { get; set; }
        public string ReferenceValue { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
