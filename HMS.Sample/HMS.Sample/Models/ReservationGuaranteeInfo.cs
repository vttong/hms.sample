﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationguaranteeinfo")]
    public class ReservationGuaranteeInfo
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public string GuaranteeRefID { get; set; }
        public string GuaranteeName { get; set; }
        public GuaranteeType GuaranteeType { get; set; }
        public GuaranteeMethod GuaranteeMethod { get; set; }
        public string GuaranteeValue { get; set; }
        public string GuaranteeDescription { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
