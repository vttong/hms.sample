﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationrateplaninfo")]
    public class ReservationRatePlanInfo
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationRoomRateID { get; set; }
        public string RatePlanName { get; set; }
        public string RatePlanCode { get; set; }
        public string RatePlanDescription { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
        public string JsonValue { get; set; }
    }
}
