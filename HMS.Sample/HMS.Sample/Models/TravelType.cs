﻿namespace HMS.Sample.Models
{
    public enum TravelType
    {
        NotSpecific = 0,
        Leisure = 1,
        Business = 2
    }
}
