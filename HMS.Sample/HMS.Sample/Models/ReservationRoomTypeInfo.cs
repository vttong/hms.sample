﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationroomtypeinfo")]
    public class ReservationRoomTypeInfo
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationRoomRateID { get; set; }
        public string RoomTypeName { get; set; }
        public string RoomTypeCode { get; set; }
        public string RoomTypeDescription { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
        public string JsonValue { get; set; }

        public IList<ReservationRoomTypeAmenityInfo> Amenities { get; set; }
    }
}
