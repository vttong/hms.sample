﻿namespace HMS.Sample.Models
{
    public enum GenderType
    {
        NotSpecific = 0,
        Male = 1,
        Female = 2
    }
}
