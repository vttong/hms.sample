﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationcancellationinfo")]
    public class ReservationCancellationInfo
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public string CancellationPolicyRefID { get; set; }
        public string CancellationPolicyName { get; set; }
        public bool IsRefundable { get; set; }
        public decimal? CancelPenaltyAmount { get; set; }
        public decimal? CancelPenaltyPercent { get; set; }
        public int CancelPenaltyNumberNights { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }
    }
}
