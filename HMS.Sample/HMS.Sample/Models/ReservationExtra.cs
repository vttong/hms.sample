﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HMS.Sample.Models
{
    [Table("reservationextra")]
    public class ReservationExtra
    {
        [Key]
        public Guid ID { get; set; }
        public Guid ReservationID { get; set; }
        public DateTime StayDate { get; set; }
        public string ExtraCode { get; set; }
        public string ExtraName { get; set; }
        public ExtraType Type { get; set; }
        public decimal RateAmount { get; set; }
        public int Quantity { get; set; }
        public string JsonValue { get; set; }
        private DateTime _createTime = DateTime.Now;
        public DateTime CreatedTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                if (value != null) _createTime = value;
            }
        }
        public DateTime? UpdatedTime { get; set; }

        public virtual IList<ReservationExtraFee> Fees { get; set; }
        public virtual IList<ReservationExtraTax> Taxes { get; set; }
    }
}
