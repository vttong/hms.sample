﻿using HMS.Sample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HMS.Sample.Services
{
    public interface IReservationService
    {
        Task<bool> Create(Reservation newRecord);

        Task<bool> Create(IEnumerable<Reservation> newRecords);

        Task<Reservation> GetRecordByID(Guid id);

        Task<IEnumerable<Reservation>> GetRecordByID(IEnumerable<Guid> ids);

        IEnumerable<Reservation> GetAll();
    }
}
