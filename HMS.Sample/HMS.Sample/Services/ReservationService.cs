﻿using HMS.Sample.Models;
using HMS.Sample.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HMS.Sample.Services
{
    public class ReservationService : IReservationService
    {
        private IRepositoryWrapper _repoWrapper;

        public ReservationService(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public async Task<bool> Create(Reservation newRecord)
        {
            try
            {
                await _repoWrapper.Reservation.CreateAsync(newRecord);

                return _repoWrapper.SaveAsync().Result > 0;
            }
            catch (Exception ex)
            {
                return await Task.FromResult(false);
            }
        }

        public async Task<bool> Create(IEnumerable<Reservation> newRecords)
        {
            try
            {
                foreach (var newRecord in newRecords)
                {
                    await _repoWrapper.Reservation.CreateAsync(newRecord);
                }

                return _repoWrapper.SaveAsync().Result > 0;
            }
            catch (Exception ex)
            {
                return await Task.FromResult(false);
            }
        }

        public async Task<Reservation> GetRecordByID(Guid id)
        {
            return await _repoWrapper.Reservation.GetReservationByIdAsync(id);
        }

        public async Task<IEnumerable<Reservation>> GetRecordByID(IEnumerable<Guid> ids)
        {
            return _repoWrapper.Reservation.FindByCondition(x => ids.Contains(x.ID));
        }

        public IEnumerable<Reservation> GetAll()
        {
            return _repoWrapper.Reservation.FindAll().AsEnumerable();
        }
    }
}
