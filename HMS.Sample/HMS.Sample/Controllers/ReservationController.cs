﻿using System;
using System.Collections.Generic;
using HMS.Sample.Models;
using HMS.Sample.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HMS.Sample.Controllers
{
    public class ReservationController : ControllerBase
    {
        private IReservationService _reservationService;

        public ReservationController(IReservationService reservationService)
        {
            _reservationService = reservationService;
        }
        
        [HttpGet]
        public string Get()
        {
            return "Connected!";
        }
        
        [HttpGet("/reservation/{id}")]
        public Reservation GetRecordByID(Guid id)
        {
            return _reservationService.GetRecordByID(id).Result;
        }

        [HttpPost]
        public IEnumerable<Reservation> GetRecordByIDs([FromBody]IEnumerable<Guid> ids)
        {
            return _reservationService.GetRecordByID(ids).Result;
        }

        [HttpPost]
        public bool CreateRecord([FromBody]Reservation reservation)
        {
            return _reservationService.Create(reservation).Result;
        }

        [HttpPost]
        public bool CreateRecords([FromBody]IEnumerable<Reservation> reservations)
        {
            return _reservationService.Create(reservations).Result;
        }
    }
}
